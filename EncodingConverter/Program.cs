﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EncodingConverter
{
    internal class Program
    {
        #region reference data
        private static readonly byte[] bom = {0xEF, 0xBB, 0xBF};

        private const int bufferSize = 1024*4;
        private const int russianCodePage = 1251;
        private const byte space = 0x20;
        private static readonly byte[] russianAlphabetSmall = Encoding.GetEncoding(russianCodePage).GetBytes("оеаинтсрвлкмдпуяыьгзбчйхжшюцщэфъё№");
        private static readonly byte[] russianAlphabetCapital = Encoding.GetEncoding(russianCodePage).GetBytes("ОЕАИНТСРВЛКМДПУЯЫЬГЗБЧЙХЖШЮЦЩЭФЪЁ№");
        private static readonly byte[] russianAlphabetSmallUtf8 = Encoding.UTF8.GetBytes("оеаинтсрвлкмдпуяыьгзбчйхжшюцщэфъё№");
        private static readonly byte[] russianAlphabetCapitalUtf8 = Encoding.UTF8.GetBytes("ОЕАИНТСРВЛКМДПУЯЫЬГЗБЧЙХЖШЮЦЩЭФЪЁ№");

        private static readonly string[] excludedDirectoryNames = {".git", "GitHooks", ".hg", ".idea", "node_modules", "assemblies", "bin", "obj", "debug", "release", "costura"};
        private static readonly HashSet<string> excludedFileNames = new HashSet<string>(new[] {
            "Dockerfile", "docker-compose", "mime"
        }, StringComparer.OrdinalIgnoreCase);
        private static readonly HashSet<string> excludedFileExtensions = new HashSet<string>(new[]
        {
            "orig",
            "exe", "dll", "pdb",
            "pdf", "txt", "vbs",
            "ttf", "woff", "eot", "svg", "otf",
            "ico", "png", "gif", "jpg", "jpeg",
            "cer", "pfx", "crt", "csr", "key", "pem", 
            "bat", "com", "ps1",
            "suo",
            "map",
            "rdlc",
            "nupkg", "json",
            "swf",
            "sh",
            "zip", "rar", "gz", "tar",
            "doc", "docx", "csv", "xls", "xlsx", "xlsm", "ppt", "pptx",
            "conf", "cnf"
            //"sln", "csproj",
            //"cs", "js", "scss", "config", "sql", "jsx", "css", "cshtml", "xslt", "xsl"
        }, StringComparer.OrdinalIgnoreCase);
        #endregion

        private static void Main(string[] args)
        {
            var paths = GetPaths(args);
            var filesToConvert = GetFiles(paths).Select(TestConvertToUtf8).ToArray();

            foreach (var fileCheckResult in filesToConvert)
            {
                Convert(fileCheckResult);
            }
        }

        private static string[] GetPaths(string[] args)
        {
           var fullPath = args.Select(Path.GetFullPath).ToArray();

            var unknownPaths = fullPath.Where(p => !File.Exists(p) && !Directory.Exists(p)).ToArray();
            if (unknownPaths.Length > 0)
            {
                var message = "Files or directories not found:\r\n" + string.Join("\r\n", unknownPaths);
                throw new Exception(message);
            }

            return fullPath;
        }

        private static string[] GetFiles(string[] paths)
        {
            var files = new List<string>();
            var itemsQueue = new Queue<string>(paths);

            while (itemsQueue.Count > 0)
            {
                var item = itemsQueue.Dequeue();

                if (Directory.Exists(item))
                {
                    if (!NeedVisit(item))
                    {
                        continue;
                    }

                    var innerItems = Directory.GetFileSystemEntries(item);
                    foreach (var innerItem in innerItems)
                    {
                        itemsQueue.Enqueue(innerItem);
                    }
                }
                else if (NeedTest(item))
                {
                    files.Add(item);
                }
            }

            return files.ToArray();
        }

        private static bool NeedVisit(string directory)
        {
            var directoryName = Path.GetFileName(directory);
            return !excludedDirectoryNames.Any(n => string.Equals(n, directoryName, StringComparison.OrdinalIgnoreCase));
        }

        private static bool NeedTest(string file)
        {
            var nameWithExtension = Path.GetFileName(file);
            if (string.IsNullOrEmpty(nameWithExtension) || nameWithExtension.StartsWith("."))
            {
                return false;
            }

            var name = Path.GetFileNameWithoutExtension(file) ?? string.Empty;
            var extension = (Path.GetExtension(file) ?? string.Empty).TrimStart('.');
            return !string.IsNullOrEmpty(name) &&
                   !string.IsNullOrEmpty(extension) &&
                   !excludedFileNames.Contains(name) &&
                   !excludedFileExtensions.Contains(extension);
        }

        private static FileCheckResult TestConvertToUtf8(string path)
        {
            if (TestStartsWithBOM(path))
            {
                return FileCheckResult.CreateNone(path);
            }

            using (var stream = new FileStream(path, FileMode.Open))
            {
                var buffer = new byte[bufferSize];
                var isWin1251 = false;
                var isUtf8WithoutBOM = false;

                while (true)
                {
                    var readCount = stream.Read(buffer, 0, bufferSize);
                    if (readCount == 0)
                        break;

                    for (var i = 0; i < readCount - 1; i++)
                    {
                        var first = buffer[i];
                        var next = buffer[i + 1];

                        isWin1251 = isWin1251 || IsRussian1251(first) && (IsRussian1251(next) || next == space || IsDigit(next));
                        isUtf8WithoutBOM = isUtf8WithoutBOM || IsRussianUtf8(first, next);
                    }

                    if (isWin1251 && isUtf8WithoutBOM)
                        return FileCheckResult.CreateAppendBOM(path);

                    if (readCount < bufferSize)
                        break;
                }

                if (isUtf8WithoutBOM)
                    return FileCheckResult.CreateAppendBOM(path);

                if (isWin1251)
                    return FileCheckResult.CreateChangeEncoding(path);
            }

            return FileCheckResult.CreateAppendBOM(path);
        }

        private static bool TestStartsWithBOM(string path)
        {
            var expectedBOM = new byte[bom.Length];

            using (var stream = new FileStream(path, FileMode.Open))
            {
                var readCount = stream.Read(expectedBOM, 0, 3);
                if (readCount != bom.Length)
                    return false;
            }

            for (var i = 0; i < bom.Length; i++)
            {
                if (bom[i] != expectedBOM[i])
                    return false;
            }

            return true;
        }

        private static bool IsDigit(byte symbol)
        {
            return symbol >= 0x30 && symbol <= 0x39;
        }

        private static bool IsRussian1251(byte symbol)
        {
            for (var i = 0; i < russianAlphabetSmall.Length; i++)
            {
                if (russianAlphabetSmall[i] == symbol || russianAlphabetCapital[i] == symbol)
                    return true;
            }

            return false;
        }

        private static bool IsRussianUtf8(byte first, byte second)
        {
            for (var i = 0; i < russianAlphabetSmall.Length*2; i=i+2)
            {
                if ((russianAlphabetSmallUtf8[i] == first && russianAlphabetCapitalUtf8[i+1] == second) ||
                    (russianAlphabetCapitalUtf8[i] == first && russianAlphabetSmallUtf8[i+1] == second) ||
                    (russianAlphabetSmallUtf8[i] == first && russianAlphabetSmallUtf8[i+1] == second) ||
                    (russianAlphabetCapitalUtf8[i] == first && russianAlphabetCapitalUtf8[i+1] == second))
                    return true;
            }

            return false;
        }


        private static void Convert(FileCheckResult fileCheckResult)
        {
            var filePath = fileCheckResult.Path;

            switch (fileCheckResult.ConvertType)
            {
                case ConvertType.AppendBOM:
                    AppendBom(filePath);
                    Console.WriteLine(filePath);
                    break;
                case ConvertType.ChangeEncoding:
                    ChangeEncoding(filePath);
                    Console.WriteLine(filePath);
                    break;
            }
        }

        private static void AppendBom(string filePath)
        {
            var tempFilePath = $"{filePath}_temp";
            using (FileStream readStream = new FileStream(filePath, FileMode.Open),
                             writeStream = new FileStream(tempFilePath, FileMode.CreateNew))
            {
                writeStream.Write(bom, 0, bom.Length);
                var buffer = new byte[bufferSize];
                while (true)
                {
                    var read = readStream.Read(buffer, 0, bufferSize);
                    if (read == 0)
                    {
                        break;
                    }

                    writeStream.Write(buffer, 0, read);
                    if (read < bufferSize)
                    {
                        break;
                    }
                }
            }

            File.Delete(filePath);
            File.Move(tempFilePath, filePath);
        }

        private static void ChangeEncoding(string filePath)
        {
            var tempFilePath = $"{filePath}_temp";
            using (FileStream readStream = new FileStream(filePath, FileMode.Open),
                             writeStream = new FileStream(tempFilePath, FileMode.CreateNew))
            {
                writeStream.Write(bom, 0, bom.Length);
                var readBuffer = new byte[bufferSize];
                while (true)
                {
                    var read = readStream.Read(readBuffer, 0, bufferSize);
                    if (read == 0)
                    {
                        break;
                    }

                    var decodedString = Encoding.GetEncoding(russianCodePage).GetString(readBuffer, 0, read);
                    var writeBuffer = Encoding.UTF8.GetBytes(decodedString);

                    writeStream.Write(writeBuffer, 0, writeBuffer.Length);
                    if (read < bufferSize)
                    {
                        break;
                    }
                }
            }

            File.Delete(filePath);
            File.Move(tempFilePath, filePath);
        }
    }

    internal enum ConvertType
    {
        None = 0,
        AppendBOM = 1,
        ChangeEncoding = 2
    }

    internal class FileCheckResult
    {
        private FileCheckResult(string path, ConvertType convertType)
        {
            Path = path;
            ConvertType = convertType;
        }

        public ConvertType ConvertType { get; }
        public string Path { get; }

        public static FileCheckResult CreateNone(string path)
        {
            return new FileCheckResult(path, ConvertType.None);
        }

        public static FileCheckResult CreateAppendBOM(string path)
        {
            return new FileCheckResult(path, ConvertType.AppendBOM);
        }

        public static FileCheckResult CreateChangeEncoding(string path)
        {
            return new FileCheckResult(path, ConvertType.ChangeEncoding);
        }
    }
}
